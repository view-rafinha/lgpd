<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package LGPD_Conformidade
 */

?>


<section class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<h2>
					ONDE NOS ENCONTRAR 
				</h2>
				
				<div class="cx-contatos">
					
					<div class="item">
						<img src="/wp-content/uploads/2022/02/lgpd-icon-6.png">
						<div>
							<a target="_blank" href="www.lethiciaferreira.adv.br">www.lethiciaferreira.adv.br </a>
							<a target="_blank" href="www.rgr.adv.br">www.rgr.adv.br </a>
						</div>
					</div>
					
					<div class="item">
						<img src="/wp-content/uploads/2022/02/lgpd-icon-7.png">
						<div>
							<a target="_blank" href="www.lethiciaferreira.adv.br">@lethiciaferreiraadv </a>
							<a target="_blank" href="www.rgr.adv.br">@rgradvconsultorest </a>
						</div>
					</div>
					
					<div class="item">
						<img src="/wp-content/uploads/2022/02/lgpd-icon-8.png">
						<div>
							<a target="_blank" href="www.lethiciaferreira.adv.br">(48) 99123-0357 </a>
							<a target="_blank" href="www.rgr.adv.br">(11) 97027-4086 </a>
						</div>
					</div>
					
					<div class="item">
						<img src="/wp-content/uploads/2022/02/lgpd-icon-9.png">
						<div>
							<a target="_blank" href="mailto:lf@lethiciaferreira.adv.br">lf@lethiciaferreira.adv.br  </a>
							<a target="_blank" href="mailto:renato@rgradv.com">renato@rgradv.com </a>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>
		<div class="row">
			<p style="text-align: center; width: 100%; font-size: 16px; margin-top: 40px;">
				<a href="https://lgpdconformidade.com/?page_id=95" target="_blank">Política de Privacidade</a>
			</p>
		</div>
	</div>
</section>


</div><!-- #page -->

<?php wp_footer(); ?>

<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script type="text/javascript">
		
	jQuery('document').ready(function(){

		jQuery(".scrolldown").click(function() {
			jQuery('html, body').animate({
				scrollTop: jQuery("#sobre").offset().top - 100
			}, 1000);
		});

	// Select all links with hashes
		jQuery('nav a[href*="#"], .page-franqueado .bx-hero a[href*="#"], .btn a[href*="#"]')
		  // Remove links that don't actually link to anything
		  .not('[href="#"]')
		  .not('[href="#0"]')
		  .click(function(event) {
			// On-page links
			if (
			  location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
			  && 
			  location.hostname == this.hostname
			) {
			  // Figure out element to scroll to
			  var target = jQuery(this.hash);
			  target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
			  // Does a scroll target exist?
			  if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				jQuery('html, body').animate({
				  scrollTop: target.offset().top - 90
				}, 1000, function() {
				  // Callback after animation
				  // Must change focus!
				  var $target = jQuery(target);
				  //$target.focus();
				  if ($target.is(":focus")) { // Checking if the target was focused
					return false;
				  } else {
					$target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
					//$target.focus(); // Set focus again
				  };
				});
			  }
			}
		  });
						
		
		
// 		jQuery('input[name=phone]').mask('(00) 00000-0000');
		
		AOS.init();
	});
	
</script>

</body>
</html>
