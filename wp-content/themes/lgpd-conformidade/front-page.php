<?php
get_header();
?>

<section class="bx-hero" data-aos="flip-left" data-aos-duration="2000">
	<?php // echo do_shortcode( '[rev_slider alias="home"]' ); ?>
	<div class="wrp-hero-mobile"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/banner-hero.jpeg" alt="Banner Hero"></div>
	<div class="wrp-hero">
		<h2>
			FAÇA UM <strong>DIAGNÓSTICO</strong> <br>
			<strong>GRATUITO</strong> PARA SEU <br> NEGÓCIO
		</h2>
		<div class="questions">
			<div class="question">&#10146; Quer entender se a LGPD é para sua empresa?</div>
			<div class="question">&#10146; O que a empresa precisa fazer para se adequar?</div>
			<div class="question">&#10146; Em caso de descumprimento, quais as consequências?</div>
			<div class="question">&#10146; Quanto tempo leva e quanto custa um processo de adequação à LGPD?</div>
			<div class="question">Se você respondeu sim a uma dessas perguntas<br>SOLICITE AGORA UM DIAGNÓTICO GRATUITO</div>
		</div>
		<?php echo do_shortcode( '[contact-form-7 id="54" title="Diagnóstico"]' ); ?>
	</div>

	<i class='scrolldown bx bx-chevron-down'></i>
</section>


<section class="wrp-sobre">
	<div class="container" data-aos="fade-up" data-aos-duration="1000">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title">
					QUEM SOMOS 
				</h2>

				<p>
					RGR Advogados em parceria com LF Advocacia oferece assessoria personalizada, potencializada pela capacidade de solução das necessidades das empresas 
					diante da obrigatoriedade da LGPD. Nas questões relacionadas a proteção de dados, possuem união que surgiu da busca de expertise com conhecimento 
					e certificações internacionais para proporcionar ao mercado segurança jurídica, com soluções eficazes para suprir as conformidades da Lei Geral
					 de Proteção de Dados, para que de forma personalizada possa potencializar os negócios dos seus clientes.
				</p>

				<div class="list-quem-somos">
					<div class="item">
						<div class="icon"><img src="<?php echo site_url('/wp-content/uploads/2022/04/imagem_2022-04-18_210712203.png') ?>" alt=""></div>
						Atendimento em todo Brasil
					</div>
					<div class="item">
						<div class="icon"><img src="<?php echo site_url('/wp-content/uploads/2022/04/imagem_2022-04-18_211200750.png') ?>" alt=""></div>
						Material exclusivo com guias de implementação
					</div>
					<div class="item">
						<div class="icon"><img src="<?php echo site_url('/wp-content/uploads/2022/04/imagem_2022-04-18_211218557.png') ?>" alt=""></div>
						+ de 20 documentos disponibilizado (contratos, termos, políticas, dentre outros)
					</div>
					<div class="item">
						<div class="icon"><img src="<?php echo site_url('/wp-content/uploads/2022/04/imagem_2022-04-18_211234309.png') ?>" alt=""></div>
						+ de 500 pessoas alcançadas em processos de implementação, treinamento e palestras;
					</div>
					<div class="item">
						<div class="icon"><img src="<?php echo site_url('/wp-content/uploads/2022/04/imagem_2022-04-18_210712203.png') ?>" alt=""></div>
						Cooautores da obra LGPD e Compliance Trabalhista (colocar a foto do nosso livro no ícone)
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="wrp-perfil">
	<div class="container">
		<div class="row align-items-center cx-lethicia" data-aos="fade-up" data-aos-duration="1000">
			<div class="col-md-3">
				<img src="<?php echo site_url('/wp-content/uploads/2022/02/lethicia.png') ?>">
			</div>
			<div class="col-md-7">
				<h2>
					LETHÍCIA FERREIRA
				</h2>

				<p>
					Advogada/Esp. Direito do Trabalho / Consultora LGPD / Membro ANPPD®/ DPO Certificada EXIN / Palestrante e Professora
				</p>
			</div>
		</div>

		<div class="row align-items-center cx-renato" data-aos="fade-up" data-aos-duration="1000">
			<div class="col-md-7 offset-md-2">
				<h2>
					RENATO GOUVÊA DOS REIS
				</h2>

				<p>
					Advogado/Esp. Direito do Trabalho / MBA Direito Empresarial e Economia/ Consultor em Governança Coporativa/ Palestrante e Professor
				</p>
			</div>			
			<div class="col-md-3">
				<img src="<?php echo site_url('/wp-content/uploads/2022/02/renato.png') ?>">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="btn">
					<a href="#diagnostico">FAÇA UM DIAGNÓSTICO GRATUITO</a>
				</div>
			</div>
		</div>

	</div>
</section>

<section class="wrp-contato" id="diagnostico" data-aos="fade-up" data-aos-duration="1000">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>
					FAÇA UM DIAGNÓSTICO GRATUITO
				</h2>
				
				<?php echo do_shortcode( '[contact-form-7 id="54" title="Diagnóstico"]' ); ?>

			</div>
		</div>
	</div>
</section>


<section class="wrp-prepare">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title" data-aos="fade-up" data-aos-duration="1000">
					PREPARE SUA EMPRESA PARA LGPD
				</h2>
			</div>

			<div class="col-md-6">
				<div class="cx-card" data-aos="fade-up" data-aos-duration="1000">
					<p>
						Proteger dados de pessoas físicas, respeitando direitos fundamentais de liberdade e privacidade
					</p>
				</div>
				<div class="cx-card" data-aos="fade-up" data-aos-duration="1000">
					<p>
						Aplicação da lei agregando valor de mercado à empresa
					</p>
				</div>
				<div class="cx-card" data-aos="fade-up" data-aos-duration="1000">
					<p>
						Aplicação de normas de privacidade e segurança da informação, considerando atividades desenvolvidas pela empresa, escopo do negócio, número de funcionários, clientes e parceiros envolvidos
					</p>
				</div>
				<div class="cx-card" data-aos="fade-up" data-aos-duration="1000">
					<p>
						Diagnóstico de adequação através de execução de mapeamento, inventário de dados e plano de ação
					</p>
				</div>
			</div>
			<div class="col-md-6" data-aos="fade-up" data-aos-duration="1000">
				<img src="<?php echo site_url('/wp-content/uploads/2022/02/prepare.png') ?>">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" data-aos="fade-up" data-aos-duration="1000">
				<div class="btn">
					<a href="#diagnostico">FAÇA UM DIAGNÓSTICO GRATUITO</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="wrp-premissas">
	<div class="container">
		<div class="row">
			<div class="col-md-12" data-aos="fade-up" data-aos-duration="1000">
				<h2 class="title">
					PREMISSAS APLICADAS
				</h2>


			</div>
		</div>
		<div class="row justify-content-center" data-aos="fade-up" data-aos-duration="1000">
			<div class="col-md-8">

				<div class="cx-card">

					<div class="item">
						<img src="<?php echo site_url('/wp-content/uploads/2022/02/lgpd-icon-1.png'); ?>"> Foco no risco do negócio
					</div>
					<div class="item">
						<img src="<?php echo site_url('/wp-content/uploads/2022/02/lgpd-icon-2.png'); ?>"> Mudança de cultura
					</div>
					<div class="item">
						<img src="<?php echo site_url('/wp-content/uploads/2022/02/lgpd-icon-3.png'); ?>"> Maximização do uso da tecnologia com segurança
					</div>
					<div class="item">
						<img src="<?php echo site_url('/wp-content/uploads/2022/02/lgpd-icon-4.png'); ?>"> Aumento da imagem reputacional 
					</div>
					<div class="item">
						<img src="<?php echo site_url('/wp-content/uploads/2022/02/lgpd-icon-5.png'); ?>"> Credibilidade no mercado
					</div>

				</div>
			</div>
		</div>
	</div>
</section>


<?php
get_footer();